#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../include/config.h"
#include "../include/xor.h"
#include "../include/utility.h"

void logger(char *str) {
  printf("[LOG] %s\n", str);
}

unsigned long filelen(FILE *fp){
  logger("Getting file length...");
  fseek(fp, 0, SEEK_END);

  int len = ftell(fp);

  rewind(fp);

  printf("[OK] Length : %d bytes\n", len);

  return len;
}
