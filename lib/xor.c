#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../include/config.h"
#include "../include/xor.h"
#include "../include/utility.h"

void compute(FileStruct *iFile, FileStruct *oFile, char *key) {
  char c;
  int i = 0;
  char *computed = NULL;
  char logStr[200];
  int keylength = strlen(key);
  unsigned long len = filelen(iFile->fp);

  computed = malloc(sizeof(char)*len);

  if ( (oFile->fp = fopen(oFile->path, "wb")) == NULL) {
    printf("Can't open file %s\n", oFile->path);
    exit(EXIT_FAILURE);
  }

  printf("--- START ---\n");

  strcpy(logStr, "Computing ");
  strcat(strcat(logStr, iFile->path), " ...");

  logger(logStr);

  for (i = 0; i < len; i++) {
    c = getc(iFile->fp);
    c ^= key[i%keylength];
    computed[i] = c;
  }

  if ( (fwrite(computed, len, 1, oFile->fp)) == 0) {
    printf("Can't write file %s\n", oFile->path);
    exit(EXIT_FAILURE);
  }

  strcpy(logStr, "File written : ");
  strcat(logStr, oFile->path);

  logger(logStr);

  printf("--- END ---\n");
}
