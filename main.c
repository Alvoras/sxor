#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "include/config.h"
#include "include/utility.h"
#include "include/xor.h"

int main(int argc, char *argv[]) {
  // STRUCTURE
  FileStruct iFile;
  FileStruct oFile;

  // COUNTER

  // DATA
  int opt;
  char *key = NULL;

  if( argc < 3 ) printf(USAGE_STR, argv[0]), exit(EXIT_FAILURE);

  while ( (opt = getopt(argc, argv, "i:o:k:")) != -1 ) {
        switch (opt) {
            case 'i':
              iFile.path = optarg;

              break;
           case 'o':
              oFile.path = optarg;

              break;
           case 'k':
              key = optarg;

              break;
           default:
              printf(USAGE_STR, argv[0]);
              exit(EXIT_FAILURE);
              break;
        }
  }

  if ( (iFile.fp = fopen(iFile.path, "rb")) == NULL) {
    printf("Can't open %s\n", iFile.path);
    exit(EXIT_FAILURE);
  }

  compute(&iFile, &oFile, key);

  fclose(iFile.fp);
  fclose(oFile.fp);
  return 0;
}
