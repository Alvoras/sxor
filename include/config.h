#define CYPHER_FLAG 0
#define DECYPHER_FLAG 1

#define USAGE_STR "Usage : %s -i <input> -o <output> -k <key>\n"

typedef struct FileStruct{
  char *path;
  FILE* fp;
} FileStruct;
