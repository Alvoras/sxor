CC=gcc
CFLAGS= -Wall
EXEC=sXor
OBJ=main.c lib/utility.c lib/xor.c

all:	$(OBJ)
	clear && $(CC) $(CFLAGS) $(OBJ) -o $(EXEC)
